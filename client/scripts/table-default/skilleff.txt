//id	name	activation_interval	transfer_effect_duration_script	use_effect_script	remove_effect_script	is_manually_cancellable	can_player_move	can_player_use_skills	can_player_normal_attack	can_player_trade	can_player_use_items	can_player_attack	can_player_be_attacked	can_player_be_item_targetable	can_player_be_skill_targetable	can_player_be_invisible	can_player_be_seen_as_himself	can_player_use_inventory	can_player_talk_to_npc	remove_effect_id	screen_effect	client_performance	client_display_id	ground_status_effect	center_display	knockout_display	special_effect_of_recipe	unknown	display_effect_when_attacking_yourself	unknown2
1	Burn	1	Skill_Effect_1_1	Skill_Effect_1_2	Skill_Effect_1_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	112	0	0	115	2	0	0
2	Healing Spring	3	Skill_Effect_2_1	Skill_Effect_2_2	Skill_Effect_2_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	268	1	0	166	-1	0	0
3	Cursed Fire	-1	Skill_Effect_3_1	Skill_Effect_3_2	Skill_Effect_3_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	158	-1	0	0
4	Poisoned	1	Skill_Effect_4_1	Skill_Effect_4_2	Skill_Effect_4_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	250	2	0	0
5	Counterguard	-1	Skill_Effect_5_1	Skill_Effect_5_2	Skill_Effect_5_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	173	0	0	174	-1	0	0
6	True Sight	-1	Skill_Effect_6_1	Skill_Effect_6_2	Skill_Effect_6_3	1	0	1	1	1	1	1	1	1	1	1	0	1	1	0	0	0	-1	176	1	0	0	-1	0	0
7	Abyss Mire	-1	Skill_Effect_7_1	Skill_Effect_7_2	Skill_Effect_7_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	240	-1	0	0
8	Spirit of Darkness	-1	Skill_Effect_8_1	Skill_Effect_8_2	Skill_Effect_8_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	306	1	0	0
9	Thunderstorm	2	Skill_Effect_9_1	Skill_Effect_9_2	Skill_Effect_9_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	7	292	0	0	0	0	0	0
10	Fog	2	Skill_Effect_10_1	Skill_Effect_10_2	Skill_Effect_10_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	8	293	0	0	0	0	0	0
11	Tornado	2	Skill_Effect_11_1	Skill_Effect_11_2	Skill_Effect_11_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	5	291	0	0	0	0	0	0
12	Whirlpool	-1	Skill_Effect_12_1	Skill_Effect_12_2	Skill_Effect_12_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	280	1	0	0	0	0	0
13	Fog	-1	Skill_Effect_13_1	Skill_Effect_13_2	Skill_Effect_13_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	283	1	0	0	0	0	0
14	Lightning Curtain	2	Skill_Effect_14_1	Skill_Effect_14_2	Skill_Effect_14_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	290	1	0	0	0	0	0
15	Taunt	-1	Skill_Effect_15_1	Skill_Effect_15_2	Skill_Effect_15_3	4	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
16	Water Mine Self Destruct	1	Skill_Effect_16_1	Skill_Effect_16_2	Skill_Effect_16_3	1	0	0	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	302	-1	0	0
17	Bull Potion	-1	Skill_Effect_17_1	Skill_Effect_17_2	Skill_Effect_17_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
18	Hairstyling	-1	Skill_Effect_18_1	Skill_Effect_18_2	Skill_Effect_18_3	1	0	0	0	0	0	0	1	1	1	1	1	0	0	0	0	0	-1	0	0	0	0	0	0	0
19	Battle Potion	-1	Skill_Effect_19_1	Skill_Effect_19_2	Skill_Effect_19_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
20	Berserk Potion	-1	Skill_Effect_20_1	Skill_Effect_20_2	Skill_Effect_20_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
21	Shield Mastery	-1	Skill_Effect_21_1	Skill_Effect_21_2	Skill_Effect_21_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
22	Tiger Roar	-1	Skill_Effect_22_1	Skill_Effect_22_2	Skill_Effect_22_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	295	-1	0	0
23	Greatsword Mastery	-1	Skill_Effect_23_1	Skill_Effect_23_2	Skill_Effect_23_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
24	Berserk	-1	Skill_Effect_24_1	Skill_Effect_24_2	Skill_Effect_24_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	118	1	0	0
25	Sudden Death 2	-1	Skill_Effect_25_1	Skill_Effect_25_2	Skill_Effect_25_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	8	0	0	0	198	2	0	0
26	Skill Forbidden	2	Skill_Effect_26_1	Skill_Effect_26_2	Skill_Effect_26_3	1	0	1	0	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	178	1	0	0
27	Fatal Strike	-1	Skill_Effect_27_1	Skill_Effect_27_2	Skill_Effect_27_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	178	-1	0	0
28	Ranger	-1	Skill_Effect_28_1	Skill_Effect_28_2	Skill_Effect_28_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
29	Tempest Boost	-1	Skill_Effect_29_1	Skill_Effect_29_2	Skill_Effect_29_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	207	1	0	0
30	Ray of Luck	-1	Skill_Effect_30_1	Skill_Effect_30_2	Skill_Effect_30_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
31	Evasion	-1	Skill_Effect_31_1	Skill_Effect_31_2	Skill_Effect_31_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
32	Spirit of Thunder	-1	Skill_Effect_32_1	Skill_Effect_32_2	Skill_Effect_32_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
33	Tornado Swirl	-1	Skill_Effect_33_1	Skill_Effect_33_2	Skill_Effect_33_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	208	1	0	0
34	Tempest Slash	-1	Skill_Effect_34_1	Skill_Effect_34_2	Skill_Effect_34_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
35	Gun Research	-1	Skill_Effect_35_1	Skill_Effect_35_2	Skill_Effect_35_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
36	Beast Strength	-1	Skill_Effect_36_1	Skill_Effect_36_2	Skill_Effect_36_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
37	Angelic Shield	-1	Skill_Effect_37_1	Skill_Effect_37_2	Skill_Effect_37_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	161	1	0	0
38	Angel Blessing	-1	Skill_Effect_38_1	Skill_Effect_38_2	Skill_Effect_38_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	166	-1	0	0
39	Cripple	-1	Skill_Effect_39_1	Skill_Effect_39_2	Skill_Effect_39_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	143	1	0	0
40	Spiritual Fire	-1	Skill_Effect_40_1	Skill_Effect_40_2	Skill_Effect_40_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	206	1	0	0
41	Energy Potion	-1	Skill_Effect_41_1	Skill_Effect_41_2	Skill_Effect_41_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
42	Harden Potion	-1	Skill_Effect_42_1	Skill_Effect_42_2	Skill_Effect_42_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
43	Conceal	5	Skill_Effect_43_1	Skill_Effect_43_2	Skill_Effect_43_3	1	1	1	1	1	1	1	1	1	1	0	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
44	Accurate Potion	-1	Skill_Effect_44_1	Skill_Effect_44_2	Skill_Effect_44_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
45	Blackout	-1	Skill_Effect_45_1	Skill_Effect_45_2	Skill_Effect_45_3	2	0	0	0	0	0	0	1	1	1	1	1	1	0	0	0	0	-1	0	0	1	253	1	0	0
46	Numb	-1	Skill_Effect_46_1	Skill_Effect_46_2	Skill_Effect_46_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	271	2	0	0
47	Invincible Potion	-1	Skill_Effect_47_1	Skill_Effect_47_2	Skill_Effect_47_3	1	0	1	1	1	1	1	0	0	0	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
48	Double Exp	-1	Skill_Effect_48_1	Skill_Effect_48_2	Skill_Effect_48_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
49	Double Drop Rate	-1	Skill_Effect_49_1	Skill_Effect_49_2	Skill_Effect_49_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
50	Intense Magic	-1	Skill_Effect_50_1	Skill_Effect_50_2	Skill_Effect_50_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	343	-1	0	0
51	Frost Shield	-1	Skill_Effect_51_1	Skill_Effect_51_2	Skill_Effect_51_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	135	-1	0	0
52	Blood Back Spirit	1	Skill_Effect_52_1	Skill_Effect_52_2	Skill_Effect_52_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
53	Firegun Mastery	-1	Skill_Effect_53_1	Skill_Effect_53_2	Skill_Effect_53_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
54	Refined Gunpowder	-1	Skill_Effect_54_1	Skill_Effect_54_2	Skill_Effect_54_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
55	Attack Weakness	-1	Skill_Effect_55_1	Skill_Effect_55_2	Skill_Effect_55_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	178	1	0	0
56	Divine Grace	-1	Skill_Effect_56_1	Skill_Effect_56_2	Skill_Effect_56_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
57	Angel Blessing	-1	Skill_Effect_57_1	Skill_Effect_57_2	Skill_Effect_57_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	178	1	0	0
58	Benediction	-1	Skill_Effect_58_1	Skill_Effect_58_2	Skill_Effect_58_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	178	1	0	0
59	Inferno Wings	-1	Skill_Effect_59_1	Skill_Effect_59_2	Skill_Effect_59_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	152	9	0	0
60	Holy Beam	-1	Skill_Effect_60_1	Skill_Effect_60_2	Skill_Effect_60_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	154	-1	0	0
61	Holy Heraldry	-1	Skill_Effect_61_1	Skill_Effect_61_2	Skill_Effect_61_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	163	-1	0	0
62	Ray of Luck	-1	Skill_Effect_62_1	Skill_Effect_62_2	Skill_Effect_62_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	169	-1	0	0
63	Ammunition Warehouse is empty	-1	Skill_Effect_63_1	Skill_Effect_63_2	Skill_Effect_63_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
64	Granary empty	-1	Skill_Effect_64_1	Skill_Effect_64_2	Skill_Effect_64_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
65	Attack forbidden	2	Skill_Effect_65_1	Skill_Effect_65_2	Skill_Effect_65_3	1	0	1	1	0	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	180	1	0	0
66	Traversing	-1	Skill_Effect_66_1	Skill_Effect_66_2	Skill_Effect_66_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
67	Hit Rate Decrease	-1	Skill_Effect_67_1	Skill_Effect_67_2	Skill_Effect_67_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	178	1	0	0
68	Enfeeble	-1	Skill_Effect_68_1	Skill_Effect_68_2	Skill_Effect_68_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	259	1	0	0
69	Sword Mastery	-1	Skill_Effect_69_1	Skill_Effect_69_2	Skill_Effect_69_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
70	Will of Steel	-1	Skill_Effect_70_1	Skill_Effect_70_2	Skill_Effect_70_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
71	Strengthen	-1	Skill_Effect_71_1	Skill_Effect_71_2	Skill_Effect_71_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
72	Deftness	-1	Skill_Effect_72_1	Skill_Effect_72_2	Skill_Effect_72_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
73	Shield Concentration	-1	Skill_Effect_73_1	Skill_Effect_73_2	Skill_Effect_73_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
74	Blood Bull	-1	Skill_Effect_74_1	Skill_Effect_74_2	Skill_Effect_74_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
75	Evasion	-1	Skill_Effect_75_1	Skill_Effect_75_2	Skill_Effect_75_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
76	Blood Frenzy	-1	Skill_Effect_76_1	Skill_Effect_76_2	Skill_Effect_76_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
77	Recuperate	-1	Skill_Effect_77_1	Skill_Effect_77_2	Skill_Effect_77_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
78	Windwalk	-1	Skill_Effect_78_1	Skill_Effect_78_2	Skill_Effect_78_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
79	Eagle's Eye	-1	Skill_Effect_79_1	Skill_Effect_79_2	Skill_Effect_79_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	214	-1	0	0
80	Vigor	-1	Skill_Effect_80_1	Skill_Effect_80_2	Skill_Effect_80_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
81	Frozen Arrow	-1	Skill_Effect_81_1	Skill_Effect_81_2	Skill_Effect_81_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	297	-1	0	0
82	Hunter Disguise	-1	Skill_Effect_82_1	Skill_Effect_82_2	Skill_Effect_82_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
83	Magical Shield	-1	Skill_Effect_83_1	Skill_Effect_83_2	Skill_Effect_83_3	1	1	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	211	-1	0	0
84	Dual Hand Weapon Bonus	-1	Skill_Effect_84_1	Skill_Effect_84_2	Skill_Effect_84_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
85	Trade Status	-1	Skill_Effect_85_1	Skill_Effect_85_2	Skill_Effect_85_3	4	0	0	0	0	1	0	1	1	1	1	1	1	0	0	0	0	-1	0	0	0	0	0	0	0
86	Tornado	-1	Skill_Effect_86_1	Skill_Effect_86_2	Skill_Effect_86_3	2	0	0	0	0	0	0	0	0	0	1	1	1	1	0	0	11	-1	0	0	1	274	-1	0	0
87	Algae Entanglement	3	Skill_Effect_87_1	Skill_Effect_87_2	Skill_Effect_87_3	2	0	0	0	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	277	0	0	0	0	0	0
88	Tail Wind	-1	Skill_Effect_88_1	Skill_Effect_88_2	Skill_Effect_88_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	287	-1	0	0
89	Break Armor	-1	Skill_Effect_89_1	Skill_Effect_89_2	Skill_Effect_89_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	306	1	0	0
90	Rousing	-1	Skill_Effect_90_1	Skill_Effect_90_2	Skill_Effect_90_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	302	-1	0	0
91	Dart	2	Skill_Effect_91_1	Skill_Effect_91_2	Skill_Effect_91_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	250	1	0	0
92	Harden	-1	Skill_Effect_92_1	Skill_Effect_92_2	Skill_Effect_92_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
93	Blood Fury	1	Skill_Effect_93_1	Skill_Effect_93_2	Skill_Effect_93_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	321	-1	0	0
94	Lair	-1	Skill_Effect_94_1	Skill_Effect_94_2	Skill_Effect_94_3	1	0	1	0	0	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	324	-1	0	0
95	Earthquake	-1	Skill_Effect_95_1	Skill_Effect_95_2	Skill_Effect_95_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	253	1	0	0
96	Kiss of Frost	-1	Skill_Effect_96_1	Skill_Effect_96_2	Skill_Effect_96_3	2	0	0	0	0	0	0	0	0	0	1	1	1	1	0	0	0	-1	0	0	0	329	2	0	0
97	Wood Bundle	3	Skill_Effect_97_1	Skill_Effect_97_2	Skill_Effect_97_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
98	Death Shriek	-1	Skill_Effect_98_1	Skill_Effect_98_2	Skill_Effect_98_3	2	0	0	0	0	0	0	1	1	1	1	1	1	1	0	0	0	-1	0	0	1	319	-1	0	0
99	Set Stall	-1	Skill_Effect_99_1	Skill_Effect_99_2	Skill_Effect_99_3	4	1	0	0	0	0	0	1	0	1	1	1	1	0	0	0	0	-1	0	0	0	0	-1	0	0
100	Repair	-1	Skill_Effect_100_1	Skill_Effect_100_2	Skill_Effect_100_3	4	1	0	0	0	0	0	1	0	1	1	1	1	0	0	0	0	-1	0	0	0	0	-1	0	0
101	Forging	-1	Skill_Effect_101_1	Skill_Effect_101_2	Skill_Effect_101_3	4	1	0	0	0	0	0	1	0	1	1	1	1	0	0	0	0	-1	0	0	0	0	-1	0	0
102	Poison strength modify	-1	Skill_Effect_102_1	Skill_Effect_102_2	Skill_Effect_102_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
103	Poition agility modify	-1	Skill_Effect_103_1	Skill_Effect_103_2	Skill_Effect_103_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
104	Poition accuracy modify	-1	Skill_Effect_104_1	Skill_Effect_104_2	Skill_Effect_104_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
105	Poition constitution modify	-1	Skill_Effect_105_1	Skill_Effect_105_2	Skill_Effect_105_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
106	Potion spirit modify	-1	Skill_Effect_106_1	Skill_Effect_106_2	Skill_Effect_106_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
107	Forging success rate increased	-1	Skill_Effect_107_1	Skill_Effect_107_2	Skill_Effect_107_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
108	Increases combination success rates	-1	Skill_Effect_108_1	Skill_Effect_108_2	Skill_Effect_108_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
109	Lantern	-1	Skill_Effect_109_1	Skill_Effect_109_2	Skill_Effect_109_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	360	1	0	0
110	Rose	-1	Skill_Effect_110_1	Skill_Effect_110_2	Skill_Effect_110_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	504	25	0	0
111	Potions to recover HP	3	Skill_Effect_111_1	Skill_Effect_111_2	Skill_Effect_111_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
112	Wine 1	-1	Skill_Effect_112_1	Skill_Effect_112_2	Skill_Effect_112_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
113	Wine 2	-1	Skill_Effect_113_1	Skill_Effect_113_2	Skill_Effect_113_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
114	Corpse Poison	3	Skill_Effect_114_1	Skill_Effect_114_2	Skill_Effect_114_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	250	1	0	0
115	Fascinate	-1	Skill_Effect_115_1	Skill_Effect_115_2	Skill_Effect_115_3	3	0	0	0	0	0	0	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	807	1	0	0
116	Black Dragon Terror	-1	Skill_Effect_116_1	Skill_Effect_116_2	Skill_Effect_116_3	1	0	0	0	0	0	0	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	319	-1	0	0
117	Black Dragon Roar	-1	Skill_Effect_117_1	Skill_Effect_117_2	Skill_Effect_117_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	158	-1	0	0
118	Crab Binding	-1	Skill_Effect_118_1	Skill_Effect_118_2	Skill_Effect_118_3	3	0	0	0	0	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	378	-1	0	0
119	Fairy March	-1	Skill_Effect_119_1	Skill_Effect_119_2	Skill_Effect_119_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	505	-1	0	0
120	Potion speed modify	-1	Skill_Effect_120_1	Skill_Effect_120_2	Skill_Effect_120_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
121	Potion ship speed modify	-1	Skill_Effect_121_1	Skill_Effect_121_2	Skill_Effect_121_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
122	Potion ship defense modify	-1	Skill_Effect_122_1	Skill_Effect_122_2	Skill_Effect_122_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
123	Spring Town Status	-1	Skill_Effect_123_1	Skill_Effect_123_2	Skill_Effect_123_3	4	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
124	Ether Clover	-1	Skill_Effect_124_1	Skill_Effect_124_2	Skill_Effect_124_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
125	Black Dragon Roar 2	-1	Skill_Effect_125_1	Skill_Effect_125_2	Skill_Effect_125_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	894	-1	0	0
126	Black Dragon Lightning Bolt	-1	Skill_Effect_126_1	Skill_Effect_126_2	Skill_Effect_126_3	1	0	0	0	0	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	887	-1	0	0
127	Party EXP	-1	Skill_Effect_127_1	Skill_Effect_127_2	Skill_Effect_127_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
128	Battle Array	-1	Skill_Effect_128_1	Skill_Effect_128_2	Skill_Effect_128_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
129	Full Body Armor	-1	Skill_Effect_129_1	Skill_Effect_129_2	Skill_Effect_129_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
130	Weightless Potion	-1	Skill_Effect_130_1	Skill_Effect_130_2	Skill_Effect_130_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
131	Pet Poison Bite	1	Skill_Effect_131_1	Skill_Effect_131_2	Skill_Effect_131_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	250	1	0	0
132	Fairy Possession A	-1	Skill_Effect_132_1	Skill_Effect_132_2	Skill_Effect_132_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	527	-1	0	0
133	Super Lollipop	-1	Skill_Effect_133_1	Skill_Effect_133_2	Skill_Effect_133_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
134	Huge Chocolate	-1	Skill_Effect_134_1	Skill_Effect_134_2	Skill_Effect_134_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
135	Deathsoul Magma Bullet	1	Skill_Effect_135_1	Skill_Effect_135_2	Skill_Effect_135_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
136	Deathsoul Acceleration	-1	Skill_Effect_136_1	Skill_Effect_136_2	Skill_Effect_136_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	922	-1	0	0
137	Deathsoul Whirlpool	-1	Skill_Effect_137_1	Skill_Effect_137_2	Skill_Effect_137_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	280	1	0	0	0	0	0
138	Encumbered Skeleton	3	Skill_Effect_138_1	Skill_Effect_138_2	Skill_Effect_138_3	1	0	0	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	921	-1	0	0
139	Skeletar Shielding	-1	Skill_Effect_139_1	Skill_Effect_139_2	Skill_Effect_139_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	912	-1	0	0
140	Deathsoul Taunt	-1	Skill_Effect_140_1	Skill_Effect_140_2	Skill_Effect_140_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	926	-1	0	0
141	Moonlight Recovery	1	Skill_Effect_141_1	Skill_Effect_141_2	Skill_Effect_141_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	920	1	0	0
142	Deathsoul Poison Dart	1	Skill_Effect_142_1	Skill_Effect_142_2	Skill_Effect_142_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	920	1	0	0
143	Deathsoul Roquet	-1	Skill_Effect_143_1	Skill_Effect_143_2	Skill_Effect_143_3	1	0	0	0	0	0	0	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	253	1	0	0
144	Whirlpool	-1	Skill_Effect_144_1	Skill_Effect_144_2	Skill_Effect_144_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	280	1	0	0	0	0	0
145	Cloud Attack	1	Skill_Effect_145_1	Skill_Effect_145_2	Skill_Effect_145_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	293	0	0	0	-1	0	0
146	Roar of Deathsoul	-1	Skill_Effect_146_1	Skill_Effect_146_2	Skill_Effect_146_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
147	Double growth rate for Fairy	-1	Skill_Effect_147_1	Skill_Effect_147_2	Skill_Effect_147_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
148	Skill - Defecate	-1	Skill_Effect_148_1	Skill_Effect_148_2	Skill_Effect_148_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	830	1	0	0
149	Skill - Undergarment	-1	Skill_Effect_149_1	Skill_Effect_149_2	Skill_Effect_149_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	831	1	0	0
150	Skill - Garment	-1	Skill_Effect_150_1	Skill_Effect_150_2	Skill_Effect_150_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	832	1	0	0
151	Skill - Coin Shower	-1	Skill_Effect_151_1	Skill_Effect_151_2	Skill_Effect_151_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	833	1	0	0
152	Skill - Fool	-1	Skill_Effect_152_1	Skill_Effect_152_2	Skill_Effect_152_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	834	1	0	0
153	Skill - Snooty	-1	Skill_Effect_153_1	Skill_Effect_153_2	Skill_Effect_153_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	835	1	0	0
154	Skill - Dumb	-1	Skill_Effect_154_1	Skill_Effect_154_2	Skill_Effect_154_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	836	1	0	0
155	Skill - Dumb	-1	Skill_Effect_155_1	Skill_Effect_155_2	Skill_Effect_155_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	837	1	0	0
156	Heart of Innocence	-1	Skill_Effect_156_1	Skill_Effect_156_2	Skill_Effect_156_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
157	Kara's Victory	-1	Skill_Effect_157_1	Skill_Effect_157_2	Skill_Effect_157_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
158	Sure-death	2	Skill_Effect_158_1	Skill_Effect_158_2	Skill_Effect_158_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
159	Flash Bomb	-1	Skill_Effect_159_1	Skill_Effect_159_2	Skill_Effect_159_3	1	0	0	0	0	0	0	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	253	1	0	0
160	Radiation	1	Skill_Effect_160_1	Skill_Effect_160_2	Skill_Effect_160_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	920	1	0	0
161	Ship Atomizer	-1	Skill_Effect_161_1	Skill_Effect_161_2	Skill_Effect_161_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	222	-1	0	0
162	Impaler	-1	Skill_Effect_162_1	Skill_Effect_162_2	Skill_Effect_162_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	306	-1	0	0
163	Ship Flamer	1	Skill_Effect_163_1	Skill_Effect_163_2	Skill_Effect_163_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
164	Boat Accelerate	-1	Skill_Effect_164_1	Skill_Effect_164_2	Skill_Effect_164_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	922	-1	0	0
165	Max HP Physical Resist Type	-1	Skill_Effect_165_1	Skill_Effect_165_2	Skill_Effect_165_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
166	Bun attack type	-1	Skill_Effect_166_1	Skill_Effect_166_2	Skill_Effect_166_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
167	Maiden Wine Spirit Type	-1	Skill_Effect_167_1	Skill_Effect_167_2	Skill_Effect_167_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
168	Fairy Possession B	-1	Skill_Effect_168_1	Skill_Effect_168_2	Skill_Effect_168_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	528	-1	0	0
169	Fairy Possession C	-1	Skill_Effect_169_1	Skill_Effect_169_2	Skill_Effect_169_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	529	-1	0	0
170	Fairy Possession D	-1	Skill_Effect_170_1	Skill_Effect_170_2	Skill_Effect_170_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	530	-1	0	0
171	Fairy Possession E	-1	Skill_Effect_171_1	Skill_Effect_171_2	Skill_Effect_171_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	531	-1	0	0
172	Fairy Possession F	-1	Skill_Effect_172_1	Skill_Effect_172_2	Skill_Effect_172_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	532	-1	0	0
173	Fairy Possession G	-1	Skill_Effect_173_1	Skill_Effect_173_2	Skill_Effect_173_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	533	-1	0	0
174	Fairy Possession H	-1	Skill_Effect_174_1	Skill_Effect_174_2	Skill_Effect_174_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	534	-1	0	0
175	Poisoned Guardian	1	Skill_Effect_175_1	Skill_Effect_175_2	Skill_Effect_175_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	8	0	0	0	198	2	0	0
176	Carrion Ball	-1	Skill_Effect_176_1	Skill_Effect_176_2	Skill_Effect_176_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	306	1	0	0
177	Noise Polluter	1	Skill_Effect_177_1	Skill_Effect_177_2	Skill_Effect_177_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	1	0	920	1	0	0
178	Earthquake Generator	-1	Skill_Effect_178_1	Skill_Effect_178_2	Skill_Effect_178_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	326	1	0	253	1	0	0
179	Chain Bullet	-1	Skill_Effect_179_1	Skill_Effect_179_2	Skill_Effect_179_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	271	-1	0	0
180	Mirage Generator	-1	Skill_Effect_180_1	Skill_Effect_180_2	Skill_Effect_180_3	1	0	1	1	1	1	1	0	0	0	1	1	1	1	0	0	0	-1	0	0	0	912	-1	0	0
181	Stealth Ship	-1	Skill_Effect_181_1	Skill_Effect_181_2	Skill_Effect_181_3	1	0	1	1	1	0	1	1	1	1	0	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
182	Radar	-1	Skill_Effect_182_1	Skill_Effect_182_2	Skill_Effect_182_3	1	0	1	1	1	1	1	1	1	1	1	0	1	1	0	0	0	-1	176	1	0	0	-1	0	0
183	Carrion Bullet	1	Skill_Effect_183_1	Skill_Effect_183_2	Skill_Effect_183_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	250	-1	0	0
184	Water Mine burning	1	Skill_Effect_184_1	Skill_Effect_184_2	Skill_Effect_184_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	115	2	0	0
185	Burning Lamb	1	Skill_Effect_185_1	Skill_Effect_185_2	Skill_Effect_185_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	115	2	0	0
186	Drag Tower	-1	Skill_Effect_186_1	Skill_Effect_186_2	Skill_Effect_186_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	297	-1	0	0
187	Healing Spring	1	Skill_Effect_187_1	Skill_Effect_187_2	Skill_Effect_187_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	166	-1	0	0
188	Devil Curse	1	Skill_Effect_188_1	Skill_Effect_188_2	Skill_Effect_188_3	1	0	1	1	1	0	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	936	-1	0	0
189	Super Consciousness	3	Skill_Effect_189_1	Skill_Effect_189_2	Skill_Effect_189_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	937	1	0	131	-1	0	0
190	Ammo Warehouse is gone	-1	Skill_Effect_190_1	Skill_Effect_190_2	Skill_Effect_190_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
191	Frozen Ring	-1	Skill_Effect_191_1	Skill_Effect_191_2	Skill_Effect_191_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	943	-1	0	0
192	Spirit of Light	-1	Skill_Effect_192_1	Skill_Effect_192_2	Skill_Effect_192_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	161	0	0	0
193	Dizzy Back Spirit	-1	Skill_Effect_193_1	Skill_Effect_193_2	Skill_Effect_193_3	2	0	0	0	0	0	0	1	1	1	1	1	1	0	0	0	0	-1	0	0	1	253	1	0	0
194	Egg Yolk Dumpling	-1	Skill_Effect_194_1	Skill_Effect_194_2	Skill_Effect_194_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	1	0	0
195	Bean Paste Dumpling	-1	Skill_Effect_195_1	Skill_Effect_195_2	Skill_Effect_195_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
196	Double Study experience	-1	Skill_Effect_196_1	Skill_Effect_196_2	Skill_Effect_196_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
197	I love my father	-1	Skill_Effect_197_1	Skill_Effect_197_2	Skill_Effect_197_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	552	-1	0	0
198	Recover HP slowly	1	Skill_Effect_198_1	Skill_Effect_198_2	Skill_Effect_198_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
199	Recover SP slowly	1	Skill_Effect_199_1	Skill_Effect_199_2	Skill_Effect_199_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
200	85BB Cow Ring	-1	Skill_Effect_200_1	Skill_Effect_200_2	Skill_Effect_200_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	574	2	0	0
201	85BB Dual Ring	-1	Skill_Effect_201_1	Skill_Effect_201_2	Skill_Effect_201_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	576	2	0	0
202	85BB Hunt Ring	-1	Skill_Effect_202_1	Skill_Effect_202_2	Skill_Effect_202_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	575	2	0	0
203	85BB Sail Ring	-1	Skill_Effect_203_1	Skill_Effect_203_2	Skill_Effect_203_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	573	2	0	0
204	85BB Seal Ring	-1	Skill_Effect_204_1	Skill_Effect_204_2	Skill_Effect_204_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	572	2	0	0
205	85BB Sacred Ring	-1	Skill_Effect_205_1	Skill_Effect_205_2	Skill_Effect_205_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	577	2	0	0
206	Navigating Lantern	-1	Skill_Effect_206_1	Skill_Effect_206_2	Skill_Effect_206_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	360	1	0	0
207	Swiftness Potion	-1	Skill_Effect_207_1	Skill_Effect_207_2	Skill_Effect_207_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
208	War Fanatic Injection	-1	Skill_Effect_208_1	Skill_Effect_208_2	Skill_Effect_208_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
209	Dragon's Armor	-1	Skill_Effect_209_1	Skill_Effect_209_2	Skill_Effect_209_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
210	Enchanted Injection 	-1	Skill_Effect_210_1	Skill_Effect_210_2	Skill_Effect_210_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
211	Shell Injection 	-1	Skill_Effect_211_1	Skill_Effect_211_2	Skill_Effect_211_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
212	Evanescence Injection 	-1	Skill_Effect_212_1	Skill_Effect_212_2	Skill_Effect_212_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
213	Accuracy Injection	-1	Skill_Effect_213_1	Skill_Effect_213_2	Skill_Effect_213_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
214	Poison strength modify	-1	Skill_Effect_214_1	Skill_Effect_214_2	Skill_Effect_214_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
215	Ship's Driving Sail	-1	Skill_Effect_215_1	Skill_Effect_215_2	Skill_Effect_215_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
216	Ship's Defensive deck	-1	Skill_Effect_216_1	Skill_Effect_216_2	Skill_Effect_216_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
217	Unable to be sentenced to prison	-1	Skill_Effect_217_1	Skill_Effect_217_2	Skill_Effect_217_3	3	1	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
218	Spirit Power	-1	Skill_Effect_218_1	Skill_Effect_218_2	Skill_Effect_218_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	578	2	0	0
219	Chiatan's Magic Book	-1	Skill_Effect_219_1	Skill_Effect_219_2	Skill_Effect_219_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
220	Super Moye Sword	-1	Skill_Effect_220_1	Skill_Effect_220_2	Skill_Effect_220_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
221	Feng's Shield	-1	Skill_Effect_221_1	Skill_Effect_221_2	Skill_Effect_221_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
222	Peter's stealthy potion	-1	Skill_Effect_222_1	Skill_Effect_222_2	Skill_Effect_222_3	1	0	1	1	1	1	1	1	1	1	0	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
223	Peter's stealthy potion 1	-1	Skill_Effect_223_1	Skill_Effect_223_2	Skill_Effect_223_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
224	Necklace Effect	-1	Skill_Effect_224_1	Skill_Effect_224_2	Skill_Effect_224_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	319	-1	0	0
225	Shock to the Heart	3	Skill_Effect_225_1	Skill_Effect_225_2	Skill_Effect_225_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
227	100% Forging successful rate	-1	Skill_Effect_227_1	Skill_Effect_227_2	Skill_Effect_227_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
228	100% CombineSuccess rate	-1	Skill_Effect_228_1	Skill_Effect_228_2	Skill_Effect_228_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
229	Lv 95 BB Set	-1	Skill_Effect_229_1	Skill_Effect_229_2	Skill_Effect_229_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	583	-1	0	0
230	Thunder Strike	-1	Skill_Effect_230_1	Skill_Effect_230_2	Skill_Effect_230_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	270	-1	0	0
231	Immortal Feet	-1	Skill_Effect_231_1	Skill_Effect_231_2	Skill_Effect_231_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	1	0	103	-1	0	0
232	Sudden Death 	-1	Skill_Effect_232_1	Skill_Effect_232_2	Skill_Effect_232_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	8	0	0	0	198	2	0	0
233	Barbecue	1	Skill_Effect_233_1	Skill_Effect_233_2	Skill_Effect_233_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	112	0	0	115	2	0	0
234	Augury: Max HP increased by shield 	-1	Skill_Effect_234_1	Skill_Effect_234_2	Skill_Effect_234_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
235	Augury: Max HP	-1	Skill_Effect_235_1	Skill_Effect_235_2	Skill_Effect_235_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
236	Augury: Max SP	-1	Skill_Effect_236_1	Skill_Effect_236_2	Skill_Effect_236_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
237	Augury: Max Movement Speed	-1	Skill_Effect_237_1	Skill_Effect_237_2	Skill_Effect_237_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
238	Allergy 	-1	Skill_Effect_238_1	Skill_Effect_238_2	Skill_Effect_238_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
239	Information Bank	-1	Skill_Effect_239_1	Skill_Effect_239_2	Skill_Effect_239_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	592	2	0	0
240	Aquarius	-1	Skill_Effect_240_1	Skill_Effect_240_2	Skill_Effect_240_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	603	1	0	0
241	Pisces	-1	Skill_Effect_241_1	Skill_Effect_241_2	Skill_Effect_241_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	604	1	0	0
242	Aries	-1	Skill_Effect_242_1	Skill_Effect_242_2	Skill_Effect_242_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	593	1	0	0
243	Taurus	-1	Skill_Effect_243_1	Skill_Effect_243_2	Skill_Effect_243_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	594	1	0	0
244	Gemini	-1	Skill_Effect_244_1	Skill_Effect_244_2	Skill_Effect_244_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	595	1	0	0
245	Cancer	-1	Skill_Effect_245_1	Skill_Effect_245_2	Skill_Effect_245_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	596	1	0	0
246	Leo	-1	Skill_Effect_246_1	Skill_Effect_246_2	Skill_Effect_246_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	597	1	0	0
247	Virgo	-1	Skill_Effect_247_1	Skill_Effect_247_2	Skill_Effect_247_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	598	1	0	0
248	Libra	-1	Skill_Effect_248_1	Skill_Effect_248_2	Skill_Effect_248_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	599	1	0	0
249	Scorpio	-1	Skill_Effect_249_1	Skill_Effect_249_2	Skill_Effect_249_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	600	1	0	0
250	Sagittarius	-1	Skill_Effect_250_1	Skill_Effect_250_2	Skill_Effect_250_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	601	1	0	0
251	Capricorn	-1	Skill_Effect_251_1	Skill_Effect_251_2	Skill_Effect_251_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	602	1	0	0
252	Goddess	-1	Skill_Effect_252_1	Skill_Effect_252_2	Skill_Effect_252_3	3	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	605	-1	0	0
253	Pirate Banner	-1	Skill_Effect_253_1	Skill_Effect_253_2	Skill_Effect_253_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	607	1	0	0
254	Force of the Devil	-1	Skill_Effect_254_1	Skill_Effect_254_2	Skill_Effect_254_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
255	The Warrior's Rage	-1	Skill_Effect_255_1	Skill_Effect_255_2	Skill_Effect_255_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	297	-1	0	0
256	The Windrider's Grace	-1	Skill_Effect_256_1	Skill_Effect_256_2	Skill_Effect_256_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	297	-1	0	0
257	Omnis Immunity 	-1	Skill_Effect_257_1	Skill_Effect_257_2	Skill_Effect_257_3	1	0	1	1	1	1	1	0	0	0	1	1	1	1	0	0	0	-1	0	0	0	0	-1	0	0
258	Eye of Precision	-1	Skill_Effect_258_1	Skill_Effect_258_2	Skill_Effect_258_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	118	1	0	0
259	Excrucio	2	Skill_Effect_259_1	Skill_Effect_259_2	Skill_Effect_259_3	1	0	1	1	1	1	0	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	936	-1	0	0
260	Curse of Weakening	-1	Skill_Effect_260_1	Skill_Effect_260_2	Skill_Effect_260_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	112	0	0	210	2	0	0
261	Bane of Waning	-1	Skill_Effect_261_1	Skill_Effect_261_2	Skill_Effect_261_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	241	-1	0	0
262	变身菇	-1	Skill_Effect_262_1	Skill_Effect_262_2	Skill_Effect_262_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	0	0	0
263	Fairy Possession I	-1	Skill_Effect_263_1	Skill_Effect_263_2	Skill_Effect_263_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	527	-1	0	0
275	VIP项链	-1	Skill_Effect_275_1	Skill_Effect_275_2	Skill_Effect_275_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	4022	1	0	0
282	霸体1级	-1	Skill_Effect_282_1	Skill_Effect_282_2	Skill_Effect_282_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
283	霸体2级	-1	Skill_Effect_283_1	Skill_Effect_283_2	Skill_Effect_283_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
284	霸体3级	-1	Skill_Effect_284_1	Skill_Effect_284_2	Skill_Effect_284_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
285	霸体4级	-1	Skill_Effect_285_1	Skill_Effect_285_2	Skill_Effect_285_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
286	霸体5级	-1	Skill_Effect_286_1	Skill_Effect_286_2	Skill_Effect_286_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
288	ExpCorruption	-1	Skill_Effect_288_1	Skill_Effect_288_2	Skill_Effect_288_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	0	1	0	0
289	圣光附体	-1	Skill_Effect_289_1	Skill_Effect_289_2	Skill_Effect_289_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	0	0	0	301	1	0	0
290	圣光沐浴	1	Skill_Effect_290_1	Skill_Effect_290_2	Skill_Effect_290_3	1	0	1	1	1	1	1	1	1	1	1	1	1	1	0	0	0	-1	268	1	0	166	-1	0	0