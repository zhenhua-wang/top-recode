function Gem_Desc_1(l) return GetResString("CO_COMMFUNC_H_00009")..": +"..tostring(4*l) end
function Gem_Desc_2(l) return GetResString("CO_COMMFUNC_H_00009")..": +"..tostring(6*l) end
function Gem_Desc_3(l) return GetResString("CO_COMMFUNC_H_00009")..": +"..tostring(10*l) end
function Gem_Desc_4(l) return GetResString("CO_COMMFUNC_H_00015")..": +"..tostring(5*l) end
function Gem_Desc_5(l) return GetResString("CO_COMMFUNC_H_00011")..": +"..tostring(5*l).."\n"..GetResString("CO_COMMFUNC_H_00022")..": +"..tostring(5*l) end
function Gem_Desc_6(l) return GetResString("CO_COMMFUNC_H_00012")..": +"..tostring(100*l).."\n"..GetResString("CO_COMMFUNC_H_00013")..": +"..tostring(100*l) end
function Gem_Desc_7(l) return GetResString("CO_COMMFUNC_H_00014")..": +"..tostring(2*l) end
function Gem_Desc_8(l) return GetResString("CO_COMMFUNC_H_00010")..": +"..tostring(10*l) end
function Gem_Desc_9(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(5*l) end
function Gem_Desc_10(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(5*l) end
function Gem_Desc_11(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(5*l) end
function Gem_Desc_12(l) return GetResString("CO_COMMFUNC_H_00001")..": +"..tostring(5*l) end
function Gem_Desc_13(l) return GetResString("CO_COMMFUNC_H_00009")..": +"..tostring(50*l).."\n"..GetResString("CO_COMMFUNC_H_00010")..": +"..tostring(50*l) end
function Gem_Desc_14(l) return GetResString("CO_COMMFUNC_H_00011")..": +"..tostring(50*l).."\n"..GetResString("CO_COMMFUNC_H_00022")..": +"..tostring(50*l) end
function Gem_Desc_15(l) return GetResString("CO_COMMFUNC_H_00012")..": +"..tostring(500*l).."\n"..GetResString("CO_COMMFUNC_H_00013")..": +"..tostring(500*l) end
function Gem_Desc_16(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(5*l) end
function Gem_Desc_17(l) return GetResString("CO_COMMFUNC_H_00011")..": +"..tostring(10*l).."\n"..GetResString("CO_COMMFUNC_H_00022")..": +"..tostring(10*l) end
function Gem_Desc_18(l) return GetResString("CO_COMMFUNC_H_00012")..": +"..tostring(200*l) end
function Gem_Desc_19(l) return GetResString("CO_COMMFUNC_H_00013")..": +"..tostring(200*l) end
function Gem_Desc_20(l) return GetResString("CO_COMMFUNC_H_00016")..": +"..tostring(10*l) end
function Gem_Desc_21(l) return GetResString("CO_COMMFUNC_H_00015")..": +"..tostring(10*l) end
function Gem_Desc_22(l) return GetResString("CO_COMMFUNC_H_00014")..": +"..tostring(10*l) end
function Gem_Desc_23(l) return GetResString("CO_COMMFUNC_H_00011")..": +"..tostring(15*l).."\n"..GetResString("CO_COMMFUNC_H_00022")..": +"..tostring(15*l) end
function Gem_Desc_24(l) return GetResString("CO_COMMFUNC_H_00012")..": +"..tostring(300*l).."\n"..GetResString("CO_COMMFUNC_H_00013")..": +"..tostring(300*l) end
function Gem_Desc_25(l) return GetResString("CO_COMMFUNC_H_00001")..": +"..tostring(8*l) end
function Gem_Desc_26(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(8*l) end
function Gem_Desc_27(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(8*l) end
function Gem_Desc_28(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(8*l) end
function Gem_Desc_29(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(8*l) end
function Gem_Desc_30(l) return GetResString("CO_COMMFUNC_H_00011")..": +"..tostring(5*l).."\n"..GetResString("CO_COMMFUNC_H_00022")..": +"..tostring(5*l) end
function Gem_Desc_31(l) return GetResString("CO_COMMFUNC_H_00012")..": +"..tostring(3*l) end
function Gem_Desc_32(l) return GetResString("CO_COMMFUNC_H_00009")..": +"..tostring(5*l).."\n"..GetResString("CO_COMMFUNC_H_00010")..": +"..tostring(5*l) end
function Gem_Desc_33(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(1*l) end
function Gem_Desc_34(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(2*l) end
function Gem_Desc_35(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(3*l) end
function Gem_Desc_36(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(4*l) end
function Gem_Desc_37(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(2*l) end
function Gem_Desc_38(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(3*l) end
function Gem_Desc_39(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(4*l) end
function Gem_Desc_40(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(2*l) end
function Gem_Desc_41(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(3*l) end
function Gem_Desc_42(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(4*l) end
function Gem_Desc_43(l) return GetResString("CO_COMMFUNC_H_00001")..": +"..tostring(2*l) end
function Gem_Desc_44(l) return GetResString("CO_COMMFUNC_H_00001")..": +"..tostring(3*l) end
function Gem_Desc_45(l) return GetResString("CO_COMMFUNC_H_00001")..": +"..tostring(4*l) end
function Gem_Desc_46(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(2*l) end
function Gem_Desc_47(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(3*l) end
function Gem_Desc_48(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(4*l) end
function Gem_Desc_49(l) return GetResString("CO_COMMFUNC_H_00001")..": +"..tostring(6*l) end
function Gem_Desc_50(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(6*l) end
function Gem_Desc_51(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(6*l) end
function Gem_Desc_52(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(6*l) end
function Gem_Desc_53(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(6*l) end
function Gem_Desc_54(l) return GetResString("CO_COMMFUNC_H_00001")..": +"..tostring(2*l) end
function Gem_Desc_55(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(2*l) end
function Gem_Desc_56(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(2*l) end
function Gem_Desc_57(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(2*l) end
function Gem_Desc_58(l) return GetResString("CO_COMMFUNC_H_00004")..": +"..tostring(2*l) end
function Gem_Desc_59(l) return GetResString("CO_COMMFUNC_H_00011")..": +"..tostring(20*l).."\n"..GetResString("CO_COMMFUNC_H_00022")..": +"..tostring(20*l) end
function Gem_Desc_60(l) return GetResString("CO_COMMFUNC_H_00012")..": +"..tostring(300*l).."\n"..GetResString("CO_COMMFUNC_H_00013")..": +"..tostring(300*l) end
function Gem_Desc_61(l) return GetResString("CO_COMMFUNC_H_00005")..": +"..tostring(3*l) end
function Gem_Desc_62(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(3*l) end
function Gem_Desc_63(l) return GetResString("CO_COMMFUNC_H_00002")..": +"..tostring(3*l) end
function Gem_Desc_64(l) return GetResString("CO_COMMFUNC_H_00003")..": +"..tostring(3*l) end
function Gem_Desc_65(l) return GetResString("CO_COMMFUNC_H_00016")..": +"..tostring(20*l) end
function Gem_Desc_66(l) return  end
function Gem_Desc_67(l) return  end
function Gem_Desc_68(l) return  end
function Gem_Desc_69(l) return  end
function Gem_Desc_70(l) return  end
function Gem_Desc_71(l) return  end