color_scheme = "blue"

color_scheme_folder = "texture/skins/"..skin_name.."/"..color_scheme

dofile(skin_folder.."/main.lua")
dofile(skin_folder.."/login.lua")
dofile(skin_folder.."/loading.lua")
dofile(skin_folder.."/NPC.lua")
dofile(skin_folder.."/Player.lua")
dofile(skin_folder.."/minimap.lua")
dofile(skin_folder.."/help.lua")
dofile(skin_folder.."/system.lua")
dofile(skin_folder.."/equip.lua")
dofile(skin_folder.."/chat.lua")
dofile(skin_folder.."/preperty.lua")
dofile(skin_folder.."/mission.lua")
dofile(skin_folder.."/coze.lua")
dofile(skin_folder.."/dialog.lua")
dofile(skin_folder.."/ship.lua")
dofile(skin_folder.."/traderoom.lua")
dofile(skin_folder.."/manage.lua")
dofile(skin_folder.."/select.lua")

--extras from new client/server source:

--Char selection list
lstChar = UI_CreateCompent( frmSelect, LIST_TYPE, "lstChar", 302, 282, 10, 32 )
UI_SetMargin( lstChar, 12, 1, 15, 0 )
UI_ListSetItemMargin( lstChar, 10, 9 )
UI_SetListRowHeight( lstChar, 23 )
UI_SetListIsMouseFollow( lstChar, TRUE)
UI_ListLoadSelectImage( lstChar, "texture/ui/SystemBotton3.tga", 79, 13, 25, 227 )
UI_SetImageAlpha( lstChar, 134 )
UI_SetListFontColor( lstChar, COLOR_BLACK, COLOR_RED )

--Mod form
-----------------------------------------------------------------------
-- 定义改名界面，该界面仅对台湾版本有效，请做好备份工作
-----------------------------------------------------------------------	
frmmod = UI_CreateForm("frmmod", TRUE, 500, 170, 0 , 0, TRUE,TRUE)
UI_ShowForm(frmmod, FALSE )
UI_SetIsDrag(frmmod, FALSE)
UI_FormSetIsEscClose(frmmod, FALSE)
UI_SetFormStyle(frmmod,1)
UI_AddFormToTemplete( frmmod, FORM_SELECT )

--加载背景
imgCreateOKNotice1 = UI_CreateCompent(frmChaNameAlter, IMAGE_TYPE,"imgCreateOKNotice1", 256, 150, 0, 0)
UI_LoadImage( imgCreateOKNotice1, "texture/ui/newshelp/login_out_01.tga", NORMAL, 256,150, 0, 0)
UI_SetImageAlpha(imgCreateOKNotice1,220)

--加载标题
labTitle = UI_CreateCompent( frmmod, LABELEX_TYPE, "labTitle", 400, 150, 10, 3 )
FORMS_LOGIN_CLU_000024 = GetResString("FORMS_LOGIN_CLU_000024")
UI_SetCaption( labTitle, FORMS_LOGIN_CLU_000024)
UI_SetTextColor( labTitle, COLOR_BLACK )
UI_SetLabelExFont( labTitle, DEFAULT_FONT, TRUE, COLOR_WHITE )

--修改面板
labText1 = UI_CreateCompent( frmmod, LABELEX_TYPE, "labText1", 400, 150, 10, 20)
FORMS_LOGIN_CLU_000025 = GetResString("FORMS_LOGIN_CLU_000025")
UI_SetCaption( labText1, FORMS_LOGIN_CLU_000025)
UI_SetTextColor( labText1, COLOR_BLACK )
UI_SetLabelExFont( labText1, DEFAULT_FONT, TRUE, COLOR_WHITE )


--新角色名
labTitle = UI_CreateCompent( frmmod, LABELEX_TYPE, "labTitle", 400, 150, 40, 30 )
FORMS_LOGIN_CLU_000026 = GetResString("FORMS_LOGIN_CLU_000026")
UI_SetCaption( labTitle, FORMS_LOGIN_CLU_000026)
UI_SetTextColor( labTitle, COLOR_BLACK )
UI_SetLabelExFont( labTitle, DEFAULT_FONT, TRUE, COLOR_WHITE )

--重新输入
labTitle = UI_CreateCompent( frmmod, LABELEX_TYPE, "labTitle", 400, 150, 40, 50 )
FORMS_LOGIN_CLU_000024 = GetResString("FORMS_LOGIN_CLU_000024")
UI_SetCaption( labTitle, FORMS_LOGIN_CLU_000024)
UI_SetTextColor( labTitle, COLOR_BLACK )
UI_SetLabelExFont( labTitle, DEFAULT_FONT, TRUE, COLOR_WHITE )

--修改框
edtname = UI_CreateCompent(frmmod, EDIT_TYPE,"edtname", 66, 14, 140, 30)
UI_SetEditMaxNum( edtname, 12 )
UI_SetEditMaxNumVisible(edtname,12)

--确定，取消按钮
btnYes = UI_CreateCompent( frmmod, BUTTON_TYPE, "btnYes", 47, 22, 50, 120 )
UI_LoadButtonImage( btnYes, color_scheme_folder.."/button.tga", 47, 22, 0, 0, TRUE )

btnLeft3d = UI_CreateCompent( frmChaNameAlter, BUTTON_TYPE, "btnLeft3d", 47, 22, 80, 120 )
UI_LoadButtonImage( btnLeft3d, color_scheme_folder.."/button.tga", 47, 22, 0, 22, TRUE )

btnRight3d = UI_CreateCompent( frmChaNameAlter, BUTTON_TYPE, "btnRight3d", 47, 22, 110, 120 )
UI_LoadButtonImage( btnRight3d, color_scheme_folder.."/button.tga", 47, 22, 0, 22, TRUE )