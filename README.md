# top-recode

### **Discord link: https://discord.gg/uwcR74E**

Objective is to recode TOP/PKO .lua files in a way the syntax becomes easier to use, or that implements features that can provide more freedom of coding.

This is only intended for testing. Everything here is meant to be used in your own computer (no website for example): including the server and client.

PKO 2.7 mod is used. My default mod is still on alpha and is not supposed to be used yet.

## Goals (ATM):
- Get it working without major bugs. It is.
- Fix warnings.
- Fix some bugs.
- More later.

## WINDOWS:

### How to install:
1. Install Microsoft Visual Studio 2019 Community Edition. Then on Visual Studio Installer (if you had it installed previously, look on Start Menu -> "Visual Studio Installer"):
    1. Check "Desktop Development with C++". Then check these items on the drop-down that appeared on the right side:
		- C++ MFC for latest ...
	2. Click "Install"/"Modify".
2. Install and configure MSQL of any version liked - recommended MSQL 2019 and the guide as follows: https://pko.coffeecup.com/. Don't do anything regarding db accounts (except sa), dbs, ip or more yet - leave that part for later.
3. Install git for windows. Select the cmd.exe options (but no options with added unix tools please).
4. Download the project:
    1. **WARNING: .bat files must have Windows' CR LF line endings - The following will break the build:**
		1. Downloading project files from the web page repository (files will come with Linux line endings).
		2. Using git on a different operating system other than Windows first (files will come with line endings of respective OS - Mac with CR, Linux with LF).
    2. Run cmd.exe with Admin priviledges (Search for "Command Prompt" and click "Run as administrator").
    3. Then cd to the folder where you want the project to be - like the following, for example (if it doesn't exist, also do mkdir C:/dev) - remember the /D parameter here:
    ```
	cd /D C:/dev
	```
    4. Then type the following to download the files:
    ```
	git clone "https://gitlab.com/deguix/top-recode"
	```
5. Compile the project;
    1. Then cd to the project folder - the following is an example with default name and following previous example:
	```
    cd top-recode
	```
    2. Type "build.bat" and add the following parameters if needed:
        1. "--debug": Use this if doing a debug build (luajit debug files replaces the release ones!)
        2. "--x64": Use this for the x64 build. Everything compiles and runs, except for the game client at this moment.
        3. "--nodb": Databases are rebuilt by default on every run. Type this so data from them don't get reset.
    3. An example (you really don't want any parameters if you're not used to this):
    ```
	build.bat
	```
6. (Only if using SQL 2000) Open all .cfg files that are in server folder, and remove any ",1433" you see.

### How to update without any other actions (if for some reason build.bat fails and online git files are newer):
1. Run a git pull:
    1. Run cmd.exe (Search for "Command Prompt" and click it).
	2. Then cd to the project folder - remember the /D parameter here - example:
	```
	cd /D C:/dev/top-recode
	```
	3. Then type the following:
	```
	git pull
	```
	
### How to update and then compile src files (no symlinks are created here):
1. Run build.bat with the --pull parameter:
    1. Run cmd.exe (Search for "Command Prompt" and click it).
	2. Then cd to the project folder - remember the /D parameter here - example:
	```
	cd /D C:/dev/top-recode
	```
	3. Then type the following:
	```
	build.bat --pull
	```

### How to use (assumes build.bat completes successfully once):
1. Start the SQL server (if it isn't by now) and run runall.bat in server folder to start all game server exe's (not applicable to TradeServer and TradeTools yet, they're still being fixed).
2. Run run.bat in client folder to run client.
3. Select the only server available, and put the user name *deguix* and pass *123456* and press Enter (it's a fresh account, no chars are there). The game should be working at this point.

## LINUX:

Copy or symlink folder from Windows. Unfortunatelly the source can only be compiled on Windows.

## Supplemental - For developers only: How to adapt mod files to use this source:
### Server lua files:
1. Make sure the scripts work on TOP2 server.
2. Copy scripts folder from the resource folder of your mod, and paste in inside this projects resource/scripts folder.
3. Rename it to whatever mod name you would like.
4. Copy initial.lua from pko_2.7 (it's a compability mod) to your recently copied mod folder.
5. Create a "maps" folder in your mod folder.
6. Create a folder for each map with its name in the "maps" folder.
7. Copy each map folder .lua files in your original files to respective map folders (that are in your "map folder).
8. (TO BE CHANGED - TODO: Give each mod independence) Copy the .txt and other files (not .lua) of your map folders to the respective map folders in the resource folder.
9. Replace all "local count = ReadByte( rpk )" with local count = ReadWord( rpk )" in ncpsdk.lua (higher item buy/sale limits).

### Client files (TO BE CHANGED - TODO: Give each mod independence):
1. Lua files have no compatibility yet (and no missions/gems yet).
2. Replace table folder of these files with your own mod's.
3. Replace any other files except lua files and system folder files with your own.

### Publishing:
1. When zipping the files, the symlinks will resolve.

## Supplemental - .clang-format file
- The .clang-format file determines the c++ formatting of the files used by Visual Studio.
- It's found in the source folder.
- Note that if you make changes to it, only the files changed by you will have the new formatting.
- To make all files have the new formatting, use the recurse_clang-format.bat found in the same folder. I don't garantee it will work with all files - Windows tends to hiccup on a couple. If you find any like that, rename them temporarily.